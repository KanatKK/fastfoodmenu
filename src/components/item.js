import React from 'react';

const Item = props => {
    return (
        <div className={props.className} onClick={props.onItemClick}>
            <h5 className="name">{props.name}</h5>
            <p className="price">Price: {props.price}</p>
        </div>
    );
};

export default Item;