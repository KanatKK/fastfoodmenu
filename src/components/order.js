import React from 'react';

const Order = props => {
    return (
        <div className="order">
            <p className="orderName">{props.name}</p>
            <p className="counter">x {props.counter}</p>
            <p className="orderPrice">{props.price}</p>
            <div className="delete" onClick={props.onTrashClick} />
        </div>
    );
};

export default Order;