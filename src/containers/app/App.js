import React, {useState} from 'react';
import './App.css';
import Item from "../../components/item";
import Order from "../../components/order";

const App = () => {
    const [items, setItems] = useState([
        {className: 'item hamburger', name:'Hamburger', price: '120', state: false},
        {className: 'item pizza', name:'Pizza', price: '600', state: false},
        {className: 'item hot-dog', name: 'Hot-Dog', price: '80', state: false},
        {className: 'item cake', name: 'Cake', price: '90', state: false},
        {className: 'item coffee', name: 'Coffee', price: '30', state: false},
        {className: 'item tea', name: 'Tea', price: '20', state: false}
    ]);

    const [orders, setOrders] = useState([]);

    const addItem = index => {
        const itemsCopy = [...items];
        const ordersCopy = [...orders];
        if (itemsCopy[index].state === true) {
            for (const key in ordersCopy) {
                if (ordersCopy[key].name === items[index].name) {
                    ordersCopy[key].counter++
                    const prices = (newIndex, price) => {
                        if(index === newIndex) {
                            ordersCopy[key].orderPrice = ordersCopy[key].orderPrice + price
                        }
                    }
                    prices(0, 120)
                    prices(1, 600)
                    prices(2, 80)
                    prices(3, 90)
                    prices(4, 30)
                    prices(5, 20)
                }
            }
        }
        if (itemsCopy[index].state === false) {
            const add = (newIndex, name, price) => {
                if(index === newIndex) {
                    ordersCopy.push({name: name, counter: 1, orderPrice: price})
                }
            };
            add(0, 'Hamburger', 120);
            add(1,'Pizza', 600);
            add(2, 'Hot-Dog', 80);
            add(3, 'Cake', 90);
            add(4, 'Coffee', 30);
            add(5, 'Tea', 20);
        }
        setOrders(ordersCopy)
        itemsCopy[index].state = true
        setItems(itemsCopy);
    };

    const deleteItem = index => {
        const ordersCopy = [...orders];
        const itemsCopy = [...items];
        for (const key in itemsCopy) {
            if (itemsCopy[key].name === ordersCopy[index].name) {
                itemsCopy[key].state = false
            }
        }
        ordersCopy.splice(index, 1);
        setItems(itemsCopy);
        setOrders(ordersCopy);
    };

    let totalPrice = () => {
        return orders.reduce((acc, order) => {
            return acc + order.orderPrice
        }, 0);
    };

    let itemsList = items.map((items, index) => {
        return(
            <Item key={index} className={items.className} name={items.name} price={items.price} onItemClick={() => addItem(index)}/>
        );
    });

    let ordersList = orders.map((orders,index) => {
        return(
            <Order key={index} name={orders.name} counter={orders.counter} price={orders.orderPrice} onTrashClick={() => deleteItem(index)}/>
        );
    });

  return(
      <div className="container">
          <div className="menu">
              <div className="orders">
                  <h4 className="ordersHeading">Order Details:</h4>
                  {ordersList}
                  <div className="empty">Order is empty</div>
                  <div className="totalPrice">Price: {totalPrice()}</div>
              </div>
              <div className="addItems">
                  <h4 className="itemsHeading">Add items:</h4>
                  <div className="items">
                      {itemsList}
                  </div>
              </div>
          </div>
      </div>
  );
};

export default App;
